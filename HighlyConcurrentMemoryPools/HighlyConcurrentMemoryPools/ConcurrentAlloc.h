#pragma once
#include "Common.h"
#include "ThreadCache.h"
#include "PageCache.h"
#include "ObjectPool.hpp"
//通过TLS，每个线程无锁地获取自己地专属ThreadCache对象
static void* ConcurrentAlloc(size_t size)
{
	if (size > MAX_BYTES)
	{
		size_t alignSize = AlignmentRules::AlignUp(size);	//对齐数
		size_t Kpage = alignSize >> PAGE_SHIFT;	//页数

		PageCache::GetInstance()->_pageMutex.lock();
		Span* span = PageCache::GetInstance()->NewSpan(Kpage);	//获取一个k页的span
		span->_objSize = size;
		PageCache::GetInstance()->_pageMutex.unlock();

		void* ptr = (void*)(span->_pageId << PAGE_SHIFT);	
		return ptr;
	}
	else
	{
		if (pTLSThreadCache == nullptr)
		{
			static ObjectPool<ThreadCache> tcPool;
			PageCache::GetInstance()->_threadMtx.lock();
			pTLSThreadCache = tcPool.New();
			PageCache::GetInstance()->_threadMtx.unlock();
			//pTLSThreadCache = new ThreadCache;
		}
		//cout << std::this_thread::get_id() << ":" << pTLSThreadCache << endl;
		return pTLSThreadCache->Allocate(size);
	}
}
//释放_freeList
static void ConcurrentFree(void* ptr)
{
	Span* span = PageCache::GetInstance()->MapObjectToSpan(ptr);
	size_t size = span->_objSize;
	if (size > MAX_BYTES)
	{
		PageCache::GetInstance()->_pageMutex.lock();
		PageCache::GetInstance()->ReleaseSpanToPageCache(span);
		PageCache::GetInstance()->_pageMutex.unlock();
	}
	else
	{
		//每一个线程都会有一个pTLSThreadCache对象
		assert(pTLSThreadCache);
		pTLSThreadCache->Deallocate(ptr, size);
	}
}