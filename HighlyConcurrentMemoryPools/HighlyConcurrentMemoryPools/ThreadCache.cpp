#pragma once
#include "ThreadCache.h"
#include "CentralCache.h"


//从中心存储中获取
void* ThreadCache::FetchFromCentralCache(size_t index, size_t AlignNum)
{
	//慢开始反馈调节算法
	//选出合适的对象申请数，慢开始
	size_t batchNum = min(_freeLists[index].MaxSize(), SizeClass::NumMoveSize(AlignNum));
	if (batchNum == _freeLists[index].MaxSize())
	{
		++_freeLists[index].MaxSize();
	}
	void* start = nullptr;
	void* end = nullptr;
	//实际能从CentralCache中获取到的对象数
	size_t actualNum = CentralCache::GetInstance()->FetchRangeObj(start, end, batchNum, AlignNum);
	//将从CentralCache中获取到的对象数给ThreadCache
	assert(actualNum >= 1);		
		
	if (actualNum == 1)
	{
		assert(start == end);
		return start;
	}
	//返回自由链表头部,并将该自由链表链接到ThreadCache对应桶中
	else
	{
		//第三个参数：有一个返回，记录实际push进入自由链表中的内存对象个数
		_freeLists[index].PushRange(FreeList::NextObj(start), end, actualNum-1);
		return start;
	}
}

void ThreadCache::ListTooLong(FreeList& list, size_t size)
{
	void* start = nullptr;
	void* end = nullptr;
	list.PopRange(start, end, list.MaxSize());
	//将批量的内存对象返还到对应的span中
	CentralCache::GetInstance()->ReleaseListToSpans(start, size);
}

//申请空间
void* ThreadCache::Allocate(size_t size)
{
	assert(size <= MAX_BYTES);		//申请的字节数不能大于了256KB
	size_t AlignNum = AlignmentRules::AlignUp(size);		//计算对齐后的所申请空间大小 
	size_t index = AlignmentRules::Index(size);		//计算在哪个桶
	if (!_freeLists[index].Empty())	//若当前自由链表有资源则优先拿释放掉的资源
		return _freeLists[index].Pop();  //取出该桶中由自由链表管理的头一个空间（头删）
	//自由链表没有就从中心缓存获取空间()
	else return FetchFromCentralCache(index, AlignNum);
}

//释放pTLSThreadCache对象（需要告诉释放哪一个桶）
void ThreadCache::Deallocate(void* ptr, size_t size)
{
	assert(ptr);
	assert(size <= MAX_BYTES);
	//计算出属于哪一个桶
	size_t index = AlignmentRules::Index(size);
	//用自由链表管理释放的内存
	_freeLists[index].Push(ptr);
	//当链表长度大于一次批量申请的内存时就开始还一段list给central cache
	if (_freeLists[index].Size() >= _freeLists[index].MaxSize())
	{
		ListTooLong(_freeLists[index], size);
	}
}


