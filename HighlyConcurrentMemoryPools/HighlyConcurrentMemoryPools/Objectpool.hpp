﻿#pragma once
#include "Common.h"

//#ifdef _WIN32
//#include <Windows.h>
//#else
//#endif
//
//inline static void* SystemAlloc(size_t kpage)
//{
//#ifdef _WIN32
//	void* ptr = VirtualAlloc(0, kpage << 13, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
//#else
//	// linux下brk mmap等
//#endif
//	if (ptr == nullptr)
//		throw std::bad_alloc();
//	return ptr;
//}

template<class T>
class ObjectPool
{
public:
	//申请空间
	T* New()
	{
		T* obj = nullptr; //定义指向所申请空间的指针
		//从自由链表中申请
		if (_freeList)
		{
			void* next = *((void**)_freeList);
			obj = (T*)_freeList;
			_freeList = next;
		}
		else
		{
			//扩容：如果剩余的内存块大小少于所申请的空间大小
			if (_SurplusBytes < sizeof(T))
			{
				_SurplusBytes = 128 * 1024;
				_memory = (char*)SystemAlloc(_SurplusBytes >> 13);
				if (_memory == nullptr)
				{
					throw std::bad_alloc();
				}
			}
			obj = (T*)_memory;
			size_t OBJSIZE = sizeof(T) < sizeof(void*) ? sizeof(void*) : sizeof(T);
			//分配空间
			_memory += OBJSIZE;
			_SurplusBytes -= OBJSIZE;
		}
		//对开辟好空间的obj进行初始化
		//（使用定位new操作符对已经分配好的内存空间进行对象的构造（初始化）操作。当我们使用new关键字创建一个对象时，会在内存中分配一块空间来存储对象的成员变量，并调用对象的构造函数来对这块内存进行初始化。但是，在某些情况下，我们可能已经手动分配了内存空间，这时候就需要使用定位new操作符来调用对象的构造函数，以便正确地初始化这块内存空间。）
		new(obj)T;
		return obj;
	}

	//将空间释放还回到自由链表中
	void Delete(T* obj)
	{
		obj->~T();
		// 头插到freeList ，指向下一个
		*((void**)obj) = _freeList;
		_freeList = obj;
	}

private:
	char* _memory = nullptr;	// 指向内存块的指针 
	size_t  _SurplusBytes = 0;		// 内存块中剩余字节数 
	void* _freeList = nullptr;  // 管理还回来的内存对象的自由链表的头指针
};

//struct TreeNode
//{
//	int _val;
//	TreeNode* _left;
//	TreeNode* _right;
//	TreeNode()
//		:_val(0)
//		, _left(nullptr)
//		, _right(nullptr)
//	{}
//};

//测试性能
//void TestObjectPool()
//{
//	// 申请释放的轮次
//	const size_t Rounds = 3;
//	// 每轮申请释放多少次
//	const size_t N = 100000;
//	size_t begin1 = clock();
//	std::vector<TreeNode*> v1;
//	v1.reserve(N);
//	for (size_t j = 0; j < Rounds; ++j)
//	{
//		for (int i = 0; i < N; ++i)
//		{
//			v1.push_back(new TreeNode);
//		}
//		for (int i = 0; i < N; ++i)
//		{
//			delete v1[i];
//		}
//		v1.clear();
//	}
//	size_t end1 = clock();
//
//	//使用定长内存池
//	ObjectPool<TreeNode> TNPool;
//	size_t begin2 = clock();
//	std::vector<TreeNode*> v2;
//	v2.reserve(N);
//	for (size_t j = 0; j < Rounds; ++j)
//	{
//		for (int i = 0; i < N; ++i)
//		{
//			v2.push_back(TNPool.New());
//		}
//		for (int i = 0; i < N; ++i)
//		{
//			TNPool.Delete(v2[i]);
//		}
//		v2.clear();
//	}
//	size_t end2 = clock();
//	cout << "new cost time:" << end1 - begin1 << endl;
//	cout << "object pool cost time:" << end2 - begin2 << endl;
//}
