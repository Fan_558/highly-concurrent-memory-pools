#pragma once
#include "Common.h"

// 单例模式（饿汉
class CentralCache
{
public:
	static CentralCache* GetInstance()
	{
		return &_inst;
	}

	//将一定数量的对象释放到span跨度
	void ReleaseListToSpans(void*& start, size_t size);
	//获取一个非空的Span
	Span* GetoneSpan(SpanList& list, size_t byte_size);

	// 从中心缓存获取一定数量的对象给thread cache
	size_t FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t size);

private:
	SpanList _spanLists[FreeListBucket];
private:
	CentralCache()
	{}
	//禁掉拷贝
	CentralCache(const CentralCache&) = delete;
	
	//声名
	static CentralCache _inst;
};