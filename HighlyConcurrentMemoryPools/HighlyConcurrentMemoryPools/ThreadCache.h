#pragma once
#include "Common.h"
// thread cache本质是由一个哈希映射的对象自由链表构成
class ThreadCache
{
public:
	// 申请和释放内存对象
	void* Allocate(size_t size);
	void Deallocate(void* ptr, size_t size);
	// 从中心缓存获取对象
	void* FetchFromCentralCache(size_t index, size_t size);
	// 释放对象时，链表过长时，回收内存回到中心缓存
	void ListTooLong(FreeList& list, size_t size);
	FreeList _freeLists[FreeListBucket];
};

	// TLS thread local storage
	//定义一个线程本地存储的类指针
	static _declspec(thread) ThreadCache* pTLSThreadCache = nullptr;
