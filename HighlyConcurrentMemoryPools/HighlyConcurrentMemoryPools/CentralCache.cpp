#include "CentralCache.h"
#include "PageCache.h"
//在.cpp中定义，如果在.h中定义会出现多次定义
CentralCache CentralCache::_inst;
//获取一个非空的span
Span* CentralCache::GetoneSpan(SpanList& list, size_t AlignNum)
{
	//从list中取出一个非空的span,遍历
	Span* it = list.Begin();
	while (it != list.End())
	{
		//存在非空的span就返回
		if (it->_freeList != nullptr)
		{
			return it;
		}
		else 
		{
			it = it->_next;
		}
	}
	bool locked = false;
	//将centralcache的桶锁解开，这样如果其它线程释放内存对象回来，就不会阻塞了
	if (list._mtx.try_lock())
	{
		locked = true;
		list._mtx.unlock();
	}
	else int i = 0;
	//没有非空的span，向PageCache中申请
	PageCache::GetInstance()->_pageMutex.lock();
	Span* span = PageCache::GetInstance()->NewSpan(SizeClass::NumMovePage(AlignNum));
	span->_isUse = true;
	span->_objSize = AlignNum;	//保存切分的内存对象的大小
	PageCache::GetInstance()->_pageMutex.unlock();
	//这里不需要立刻立刻将该线程的桶锁给续上呢，不用，因为只有此线程是拿到这个span的，其它线程没有
	//计算大块内存的起始地址以及字节数
	char* start = (char*)(span->_pageId << PAGE_SHIFT);		//起始地址
	size_t Bytes = span->_n << PAGE_SHIFT;
	char* end = start + Bytes;
	//将一大块从PageCache中申请的内存切分成由一个span指向的自由链表
	span->_freeList = start;
	start += AlignNum;
	void* tail = span->_freeList;
	int i = 1;
	while (start < end)
	{
		++i;
		FreeList::NextObj(tail) = start;
		tail = start;
		start += AlignNum;
	}
	//cout << i << endl;
	FreeList::NextObj(tail) = nullptr;

	if (locked)
	{
		list._mtx.lock();
	}
	//将span挂到桶里面去
	list.PushFront(span);
	return span;
}

//从中心缓存中申请
size_t CentralCache::FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t AlignNum)
{
	//根据对齐规则选择从哪一个桶拿（threadcache与centralcache的对齐规则相同）
	size_t index = AlignmentRules::Index(AlignNum);
	//加桶锁（由于centralcache只有一个，threadcache向centralcache申请内存时可能面临着多个线程向centralcache同一个桶申请）
	_spanLists[index]._mtx.lock();
	Span* span = CentralCache::GetoneSpan(_spanLists[index], AlignNum);
	assert(span);	//保证span不为空
	assert(span->_freeList);	//保证span对象中自由链表_freeList不为空
	start = span->_freeList;
	end = start;
	//返回actual个对象，有多少返回多少
	size_t actualNum = 1;
	size_t i = 0;
	while (i < batchNum - 1 && FreeList::NextObj(end) != nullptr)
	{
		end = FreeList::NextObj(end);
		i++;
		actualNum++;
	}
	//将分配剩余的对象重新挂在span中
	span->_freeList = *(void**)end;
	*(void**)end = nullptr;
	span->_useCount += actualNum;

	_spanLists[index]._mtx.unlock();
	return actualNum;
}

void CentralCache::ReleaseListToSpans(void*& start, size_t size)
{ 
	size_t index = AlignmentRules:: Index(size);
	//将threadcache上的自由链表上的每一个内存对象归还给对应的span中
	_spanLists[index]._mtx.lock();
	while (start)
	{
		//保存下一个
		void* next = FreeList::NextObj(start);
		//获取span与自由链表中内存对象的映射关系
		Span* span = PageCache::GetInstance()->MapObjectToSpan(start);
		//if (start == nullptr)
		//{
		//	int x = 0;
		//}
		//头插
		FreeList::NextObj(start) = span->_freeList;
		span->_freeList = start;
		span->_useCount--;		//更新分配给threadcache的计数
		//全部内存对象已经归还，此时可以将span归还给下一层pagecache中
		if (span->_useCount == 0)
		{
			//将此span从原有的链表中解除
			_spanLists[index].Erase(span);
			span->_freeList = nullptr;
			span->_prev = nullptr;
			span->_next = nullptr;
			//解桶锁，因为此span要返还给下一层，已经不用了
			_spanLists[index]._mtx.unlock();
			PageCache::GetInstance()->_pageMutex.lock();
			PageCache::GetInstance()->ReleaseSpanToPageCache(span);
			PageCache::GetInstance()->_pageMutex.unlock();
			_spanLists[index]._mtx.lock();
		}

		//进行下一个内存对象的归还
		start = next;
	}
	_spanLists[index]._mtx.unlock();
}