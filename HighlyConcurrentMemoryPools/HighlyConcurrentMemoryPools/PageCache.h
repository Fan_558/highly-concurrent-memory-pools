#pragma once
#include "Common.h"
#include "Objectpool.hpp"
#include "PageMap.hpp"

//单例模式：饿汉
class PageCache
{
public:
	static PageCache* GetInstance()
	{
		return &_sInst;
	}

	//获取一个K页的span
	Span* NewSpan(size_t K);

	//获取从对象到span的映射
	Span* MapObjectToSpan(void* obj);

	//释放空闲span回到pagecache，并合并相邻的span
	void ReleaseSpanToPageCache(Span* span);

	std::mutex _pageMutex;
	std::mutex _threadMtx;	//访问定长内存池的时候加锁
private:
	SpanList _spanLists[NPAGES];
	ObjectPool<Span> _spanPool;
	//std::unordered_map<PAGE_ID, Span*> _idSpanMap;
	TCMalloc_PageMap1<32 - PAGE_SHIFT> _idSpanMap;

private:
	PageCache()
	{}

	
	//防拷贝
	PageCache(const PageCache&) = delete;
	static PageCache _sInst;
};