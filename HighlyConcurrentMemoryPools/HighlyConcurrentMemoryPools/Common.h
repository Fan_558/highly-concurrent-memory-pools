﻿#pragma once
#include <iostream>
#include <assert.h>
#include <thread>
#include <mutex>
#include <unordered_map>
#include <vector>
#include <time.h>


using std::cout;
using std::endl;


#ifdef _WIN32
#include <Windows.h>
#else
#endif


inline static void* SystemAlloc(size_t kpage)
{
#ifdef _WIN32
	void* ptr = VirtualAlloc(0, kpage << 13, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#else
	// linux下brk mmap等
#endif
	if (ptr == nullptr)
		throw std::bad_alloc();
	return ptr;
}

inline static void SystemFree(void* ptr)
{
#ifdef _WIN32
	VirtualFree(ptr, 0, MEM_RELEASE);
#else
	// sbrk unmmap等
#endif
}


#ifdef _WIN64
typedef unsigned long long PAGE_ID;
#elif _WIN32
typedef size_t PAGE_ID;
#else
//linux
#endif

//给定最大申请
static const size_t MAX_BYTES = 256 * 1024;
//自由链表上哈希桶的个数
static const size_t FreeListBucket = 208;
static const size_t NPAGES = 129;		//让桶号与页号对应，不使用0号桶，申请单个对象最大时256KB，128页可以被切成4个256KB的对象，足够了，当然也可以挂更大的span（根据具体需要来
static const size_t PAGE_SHIFT = 13;	//将一页定义为8K，也就是2^13

// 管理小对象的自由链表
// 管理切分好的小对象的自由链表
class FreeList
{
public:
	//释放空间
	void Push(void* obj)
	{
		assert(obj);
		//让obj指向的空间与原自由链表链接
		*(void**)obj = _freeList;
		//让_freeList指向头部
		_freeList = obj;
		++_size;
	}

	//将自由链表链接到ThreadCache的桶中
	void PushRange(void* start, void* end, size_t n)
	{
		assert(start);
		assert(end);
		//头插
		NextObj(end) = _freeList;
		_freeList = start;
		_size += n;
	}

	//将指定数量的内存对象从自由链表中删除
	void PopRange(void*& start, void*& end, size_t n)
	{
		assert(n <= _size);
		start = _freeList;
		end = start;
		for (size_t i = 0; i < n - 1; i++)
		{
			end = NextObj(end);
		}
		//让_freeList链接剩余的内存对象
		_freeList = NextObj(end);
		NextObj(end) = nullptr;
		_size -= n;
	}

	//分配空间
	void* Pop()
	{
		assert(_freeList);
		void* obj = _freeList;
		_freeList = *(void**)_freeList;
		--_size;
		return obj;
	}
	//判断自由链表是否为空
	bool Empty()
	{
		return _freeList == nullptr;
	}

	size_t& MaxSize()
	{
		return _maxSize;
	}

	size_t Size()
	{
		return _size;
	}


	//返回下一个节点
	static void*& NextObj(void* obj)
	{
		return *(void**)obj;
	}

private:
	void* _freeList = nullptr;	//指向自由链表的指针
	size_t _maxSize = 1;	
	size_t _size = 0;

};


//计算对象大小的对齐映射规则
class AlignmentRules
{
public:
	// 整体控制在最多10%左右的内碎片浪费
	// [1,128] 8byte对齐       freelist[0,16)
	// [128+1,1024] 16byte对齐   freelist[16,72)
	// [1024+1,8*1024] 128byte对齐   freelist[72,128)
	// [8*1024+1,64*1024] 1024byte对齐     freelist[128,184)
	// [64*1024+1,256*1024] 8*1024byte对齐   freelist[184,208)
	
	//获取对齐后的字节数
	static inline size_t _AlignUp(size_t size, size_t AlignNum)
	{
		//对齐后的字节数
		size_t alignSize = 0;
		if (size % AlignNum != 0)
		{
			//向上取整
			alignSize = (size / AlignNum + 1) * AlignNum;
		}
		else
		{
			alignSize = size;
		}
		return alignSize;
	}

	//（获取对齐后的分配内存字节数大小）
	//static inline size_t _AlignUp(size_t size, size_t AlignNum)
	//{
	//	return ((size + AlignNum - 1) & ~(AlignNum - 1));
	//}

	//划分对齐范围
	static inline size_t AlignUp(size_t size)
	{
		if (size < 128)
		{
			return _AlignUp(size, 8);
		}
		else if (size <= 1024)
		{
			return _AlignUp(size, 16);
		}
		else if (size <= 64*1024)
		{
			return _AlignUp(size, 128);
		}
		else if (size <= 128*1024)
		{
			return _AlignUp(size, 1024);
		}
		else if (size <= 256*1024)
		{
			return _AlignUp(size, 8*1024);
		}
		else
		{
			return _AlignUp(size, 1 << PAGE_SHIFT);
		}
	}

	//对齐后的申请空间的大小， 对齐数
	static inline size_t _Index(size_t bytes, size_t align)
	{
		if (bytes % align == 0)
		{
			return bytes / align - 1;
		}
		else
		{
			return bytes / align;
		}
	}

	//static inline size_t _Index(size_t AlignNum, size_t align_shift)
	//{
	//	return ((AlignNum + (1 << align_shift) - 1) >> align_shift) - 1;
	//}
	
	//计算在自由链表上的第几个桶
	static inline size_t Index(size_t Bytes)
	{
		assert(Bytes <= MAX_BYTES);
		// 每个桶有多少个节点
		static int group_array[4] = { 16, 56, 56, 56 };
		if (Bytes <= 128) {
			return _Index(Bytes, 8);
		}
		else if (Bytes <= 1024) {
			return _Index(Bytes - 128, 16) + group_array[0];
		}
		else if (Bytes <= 8 * 1024) {
			return _Index(Bytes - 1024, 128) + group_array[1] + group_array[0];
		}
		else if (Bytes <= 64 * 1024) {
			return _Index(Bytes - 8 * 1024, 1024) + group_array[2] + group_array[1] + group_array[0];
		}
		else if (Bytes <= 256 * 1024) {
			return _Index(Bytes - 64 * 1024, 8192) + group_array[3] + group_array[2] + group_array[1] + group_array[0];
		}
		else {
			assert(false);
		}
		return -1;
	}
};

//管理以页为单位的大块内存
struct Span
{
	//给缺省值，可以不用提供构造函数
	PAGE_ID _pageId = 0;        //大块内存起始页的页号(从堆上分配内存的起始地址
	size_t _n = 0;              //页的数量

	Span* _next = nullptr;      //双链表结构
	Span* _prev = nullptr;

	size_t _objSize = 0;	//切好的小对象的大小
	size_t _useCount = 0;       //切好的小块内存，被分配给thread cache的计数
	void* _freeList = nullptr;  //切好的小块内存的自由链表
	bool _isUse = false;	//是否在被使用
};


//带头双向循环链表
class SpanList
{
public:
	//初始化双向链表
	SpanList()
	{
		//初始化头节点
		_head = new Span;
		_head->_next = _head;
		_head->_prev = _head;
	}
	//头插
	void Insert(Span* pos, Span* newSpan)
	{
		assert(pos);
		assert(newSpan);

		Span* prev = pos->_prev;
		prev->_next = newSpan;
		newSpan->_prev = prev;
		newSpan->_next = pos;
		pos->_prev = newSpan;
	}
	//头删
	void Erase(Span* pos)
	{
		assert(pos);
		assert(pos != _head);
		Span* prev = pos->_prev;
		Span* next = pos->_next;
		prev->_next = next;
		next->_prev = prev;
		//不需要真正delete该pos处的span，可能需要还给pagecache
	}
	Span* Begin()
	{
		//if (_head == nullptr)
		//{
		//	int i = 0;
		//}
		return _head->_next;
	}
	Span* End()
	{
		return _head;
	}
	//头插
	void PushFront(Span* span)
	{
		//if (_head->_next == nullptr)
		//{
		//	int i = 0;
		//}
		Insert(Begin(), span);
	}
	//头删
	Span* PopFront()
	{
		Span* front = _head->_next;
		Erase(front);
		return front;
	}
	bool Empty()
	{
		return _head->_next == _head;
	}
private:
	Span* _head;
public:
	std::mutex _mtx; //桶锁
};


class SizeClass
{
public:
	//thread cache一次从central cache获取对象的上限
	static size_t NumMoveSize(size_t size)
	{
		assert(size > 0);

		//对象越小，计算出的上限越高
		//对象越大，计算出的上限越低
		size_t num = MAX_BYTES / size;
		if (num < 2)
			num = 2;
		if (num > 512)
			num = 512;

		return num;
	}

	static size_t NumMovePage(size_t size)
	{
		size_t num = NumMoveSize(size);
		size_t npage = num * size;

		npage >>= PAGE_SHIFT;
		if (npage == 0)
			npage = 1;

		return npage;
	}
};